//
//  RegisterationViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 05/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift

class RegisterationViewController: UIViewController, UITextFieldDelegate, UIActionSheetDelegate {

    
    // MARK:- IB Outlets
    
    @IBOutlet weak var masterScrollView: UIScrollView!
    @IBOutlet weak var firstNameTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var userNameTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var countryCodeTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var relationShipTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var genderTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var signUpBtnOutlet: UIButton!
    @IBOutlet weak var loginBtnOutlet: UIButton!
    @IBOutlet weak var userNameValidImageView: UIImageView!
    @IBOutlet weak var passwordValidImageView: UIImageView!
    @IBOutlet weak var confirmPasswordValidImageView: UIImageView!
    
    // MARK:- Local Variables
    
    var registrationDict = [String:Any]()
    var isUserNameValid: Bool!
    var isPasswordValid: Bool!
    var isConfirmPasswordValid: Bool!
    var countryCode = String()
    
    // MARK:- Default Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BG.png")!)
        
        firstNameTxtFieldOutlet.delegate = self
        lastNameTxtFieldOutlet.delegate = self
        userNameTxtFieldOutlet.delegate = self
        countryCodeTxtFieldOutlet.delegate = self
        phoneNumberTxtFieldOutlet.delegate = self
        genderTxtFieldOutlet.delegate = self
        relationShipTxtFieldOutlet.delegate = self
        passwordTxtFieldOutlet.delegate = self
        confirmPasswordTxtFieldOutlet.delegate = self
        
        signUpBtnOutlet.layer.cornerRadius = 5;
        signUpBtnOutlet.clipsToBounds = true;
        
        userNameValidImageView.isHidden = true
        passwordValidImageView.isHidden = true
        confirmPasswordValidImageView.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let countryValuesCount = SharedVariables.sharedInstance.selectedCountryDict.values
        
        if countryValuesCount.count > 0 {
            
            let flagString = IsoCountries.flag(countryCode: SharedVariables.sharedInstance.selectedCountryDict["iso"] as! String)

            print(flagString)
        
            let countryText = "\(flagString) \(String(describing: SharedVariables.sharedInstance.selectedCountryDict["nicename"]!)) +\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))"

            countryCode = "\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))"
            countryCodeTxtFieldOutlet.text = countryText
        }
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    // MARK:- Custom Methods
    
    func countryCodeTapped() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectCountryViewController") as! SelectCountryViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func genderTapped() {
        
        let alertController = UIAlertController(title: "Gender", message: "", preferredStyle: .actionSheet)
        
        let maleButton = UIAlertAction(title: "Male", style: .default, handler: { (action) -> Void in
            print("Male")
            self.genderTxtFieldOutlet.text = "Male"
        })
        let  femaleButton = UIAlertAction(title: "Female", style: .default, handler: { (action) -> Void in
            print("Female")
            self.genderTxtFieldOutlet.text = "Female"
        })
        alertController.addAction(maleButton)
        alertController.addAction(femaleButton)
        self.navigationController!.present(alertController, animated: true, completion: {
            
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    @objc func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func relationShipTapped() {
        
        let alertController = UIAlertController(title: "RelationShip Status", message: "", preferredStyle: .actionSheet)
        
        let singleButton = UIAlertAction(title: "Single", style: .default, handler: { (action) -> Void in
            print("Single")
            self.relationShipTxtFieldOutlet.text = "Single"
        })
        let  complecatedButton = UIAlertAction(title: "Complicated", style: .default, handler: { (action) -> Void in
            print("Complicated")
            self.relationShipTxtFieldOutlet.text = "Complicated"
        })
        let relationShipButton = UIAlertAction(title: "In a Relationship", style: .default, handler: { (action) -> Void in
            print("In a Relationship")
            self.relationShipTxtFieldOutlet.text = "In a Relationship"
        })
        let  noneButton = UIAlertAction(title: "None", style: .default, handler: { (action) -> Void in
            print("None")
            self.relationShipTxtFieldOutlet.text = "None"
        })
        alertController.addAction(singleButton)
        alertController.addAction(complecatedButton)
        alertController.addAction(relationShipButton)
        alertController.addAction(noneButton)
        self.navigationController!.present(alertController, animated: true, completion: {
            
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    // MARK:- Button Actions

    @available(iOS 10.0, *)
    @IBAction func signUpBtnAction(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fcmToken = appDelegate.fcmToken
        
        if let firstName = firstNameTxtFieldOutlet.text, firstName.count >= 8 && firstName.count <= 13 {
            
            if let lastName = lastNameTxtFieldOutlet.text, lastName.count >= 8 && lastName.count <= 13{
                
                if isUserNameValid == true{
                    
                    if countryCode.count > 0 {
                        
                       if let phoneNumber = phoneNumberTxtFieldOutlet.text, phoneNumber.count >= 10 && phoneNumber.count <= 13{
                        
                        if let gender = genderTxtFieldOutlet.text, gender.count > 0 {
                            
                            if let relationShip = relationShipTxtFieldOutlet.text, relationShip.count > 0 {
                                
                                if isPasswordValid == true
                                {
                                    if isConfirmPasswordValid == true
                                    {
                                        WebServiceHandler.sharedInstance.registration(FirstName: firstNameTxtFieldOutlet.text!, LastName: lastNameTxtFieldOutlet.text!, UserName: userNameTxtFieldOutlet.text!, PhoneNumber: "\(countryCode)\(passwordTxtFieldOutlet.text!)", Gender: genderTxtFieldOutlet.text!, RelationShipStatus: relationShipTxtFieldOutlet.text!, Password: passwordTxtFieldOutlet.text!, FCMKey: fcmToken, DeviceType: "Ios", viewController: self)
                                        
                                    }
                                    else{
                                        Utilities.showAlertView("Enter Valid Confirm Password", onView: self)
                                    }
                                }
                                else{
                                    Utilities.showAlertView("Enter Valid Password", onView: self)
                                }
                                
                            }
                            else{
                                Utilities.showAlertView("Enter Valid Relationship Status", onView: self)
                            }
                            
                        }
                        else{
                            Utilities.showAlertView("Enter Valid Gender", onView: self)
                        }
                        
                    }
                       else{
                        Utilities.showAlertView("Enter Valid Phone Number", onView: self)
                        }
                }
                    else{
                        Utilities.showAlertView("Enter Valid Country code", onView: self)
                    }
            }
                else{
                    Utilities.showAlertView("Enter Valid Username", onView: self)
                }
        }
            else{
                Utilities.showAlertView("Enter Valid Lastname", onView: self)
            }
    }
        else{
            Utilities.showAlertView("Enter Valid Firstname", onView: self)
        }
    }
    
    @IBAction func logInBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
//        WebServiceHandler.sharedInstance.sendOtp(viewController: self, authkey: "", message: "Your verification code is ##OTP##.", sender: "OTPSMS", mobile: "919952739312")

    
    }
    // MARK:- Text Field Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == countryCodeTxtFieldOutlet {
            
            self.countryCodeTapped()
            return false
        }
        else if textField == genderTxtFieldOutlet {
            
            self.genderTapped()
            return false
        }
        else if textField == relationShipTxtFieldOutlet {
            
            self.relationShipTapped()
            return false
        }
        else
        {
        return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        
         if textField == userNameTxtFieldOutlet {
            
            WebServiceHandler.sharedInstance.userNameCheck(name: textField.text!, viewController: self)
        }
         else if textField == passwordTxtFieldOutlet {
            
            if let password = textField.text, password.count >= 8 {
                passwordValidImageView.isHidden = false
                isPasswordValid = true
            }
            else
            {
                passwordValidImageView.isHidden = true
                isPasswordValid = false
            }
        }
         else if textField == confirmPasswordTxtFieldOutlet {
            
            if let confirmPassword = textField.text, confirmPassword.count >= 8 {
                
                if confirmPasswordTxtFieldOutlet.text == passwordTxtFieldOutlet.text
                {
                    confirmPasswordValidImageView.isHidden = false
                    isConfirmPasswordValid = true
                }
                else
                {
                    confirmPasswordValidImageView.isHidden = true
                    isConfirmPasswordValid = false
                }
            }
            else
            {
                confirmPasswordValidImageView.isHidden = true
                isConfirmPasswordValid = false
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        if textField == firstNameTxtFieldOutlet {
            
            lastNameTxtFieldOutlet.becomeFirstResponder()
        }
        else if textField == lastNameTxtFieldOutlet {
            
            userNameTxtFieldOutlet.becomeFirstResponder()
        }
        else if textField == userNameTxtFieldOutlet {
            
            textField.resignFirstResponder()
            WebServiceHandler.sharedInstance.userNameCheck(name: textField.text!, viewController: self)
            self.countryCodeTapped()
        }
        else if textField == countryCodeTxtFieldOutlet {
            
            
        }
        else if textField == phoneNumberTxtFieldOutlet {
            
            textField.resignFirstResponder()
            self.genderTapped()
        }
        else if textField == genderTxtFieldOutlet {
            
            
        }
        else if textField == relationShipTxtFieldOutlet {
            
            self.relationShipTapped()
        }
        else if textField == passwordTxtFieldOutlet {
            
            if let password = textField.text, password.count >= 8 {
                passwordValidImageView.isHidden = false
                isPasswordValid = true
            }
            else
            {
               passwordValidImageView.isHidden = true
                isPasswordValid = false
            }
            
            confirmPasswordTxtFieldOutlet.becomeFirstResponder()
        }
        else if textField == confirmPasswordTxtFieldOutlet {
            
            if let confirmPassword = textField.text, confirmPassword.count >= 8 {
                
                if confirmPasswordTxtFieldOutlet.text == passwordTxtFieldOutlet.text
                {
                    confirmPasswordValidImageView.isHidden = false
                    isConfirmPasswordValid = true
                }
                else
                {
                    confirmPasswordValidImageView.isHidden = true
                    isConfirmPasswordValid = false
                }
            }
            else
            {
                confirmPasswordValidImageView.isHidden = true
                isConfirmPasswordValid = false
            }
            confirmPasswordTxtFieldOutlet.resignFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == firstNameTxtFieldOutlet || textField == lastNameTxtFieldOutlet {
        
         return Utilities.firstAndLastNameVadidation(string, CharactersInRange: range, textField: textField)
            
        }
        else if textField == userNameTxtFieldOutlet
        {
            if string == " "{
                return false
            }
            else{
                return Utilities.userNameVadidation(string, CharactersInRange: range, textField: textField)
            }
        }
        else if textField == passwordTxtFieldOutlet
        {
            if string == " "{
                return false
            }
            else{
                return Utilities.userNameVadidation(string, CharactersInRange: range, textField: textField)
            }
        }
        else if textField == confirmPasswordTxtFieldOutlet
        {
            if string == " "{
                return false
            }
            else{
                return Utilities.userNameVadidation(string, CharactersInRange: range, textField: textField)
            }
        }
        else
        {
            return true
        }
    }
    
    // MARK:- Api Response Methods
    
    func didReceiveUserNameCheck(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            userNameValidImageView.isHidden = false
            isUserNameValid = true
        }else{
            userNameValidImageView.isHidden = true
            isUserNameValid = false
        }
    }
    func didReceiveRegistrationResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
//            WebServiceHandler.sharedInstance.sendOtp(viewController: self, authkey: "", message: "Your verification code is ##OTP##.", sender: "OTPSMS", mobile: "\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))\(phoneNumberTxtFieldOutlet.text!)")
            
        }else{
            
            Utilities.showAlertView("Registration Failed", onView: self)
        }
    }
    func didReceiveOtpResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let status = responseDict["type"] as! String
        if status == "success" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
        }
    }
    
}
