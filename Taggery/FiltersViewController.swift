//
//  FiltersViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 07/03/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Cloudinary

class FiltersViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var filterImageViewOutlet: UIImageView!
    @IBOutlet weak var filtersCollectionView: UICollectionView!
    @IBOutlet weak var nextBtnOutlet: UIButton!
    
    var CIFilterNames = [
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CISepiaTone"
    ]
    
    var artFilters = ["art:al_dente","art:athena","art:audrey","art:aurora","art:daguerre","art:eucalyptus","art:fes","art:frost","art:hairspray","art:hokusai","art:incognito","art:linen","art:peacock","art:primavera","art:quartz","art:red_rock","art:refresh","art:sizzle","art:sonnet","art:ukulele","art:zorro"]
    
    
   // var imageName = String()
    //var cloudImageUrl = String()
    var imageUrl:URL!
    var fetchedUrl:URL!
    var fetchedImageName = String()
    var selectedImage = UIImage()
    var selectedImageData = Data()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filtersCollectionView.delegate = self
        filtersCollectionView.dataSource = self
        
        filtersCollectionView.backgroundColor = UIColor.clear
        
        Utilities.setBackgroungImage(self.view)
        
       // cloudinary.createUrl().generate("sample.jpg")
        
        // returns: http://res.cloudinary.com/demo/image/upload/sample.jpg
        self.loadCloudFilters()
    }
    
    func loadCloudFilters() {
        
        filterImageViewOutlet.image = UIImage(data:selectedImageData,scale:1.0)
        
        let config = CLDConfiguration(cloudName: "venkicloudinary", apiKey: "s9Ls7hcfiRgcHiEaxgXVaelC89k")
        let cloudinary = CLDCloudinary(configuration: config)
        //    let request = cloudinary.createUploader().upload(url: imageUrl, uploadPreset: "Sample_preset")
        let uploader = cloudinary.createUploader()
        uploader.upload(data: selectedImageData, uploadPreset: "Sample_preset")
        { result, error in
            
            let fetch = result?.url
            print(fetch!)
            self.fetchedUrl = URL(string: fetch!)
            self.fetchedImageName = self.fetchedUrl.lastPathComponent
            print("\(self.fetchedImageName)")
            
            self.filtersCollectionView.reloadData()
            
        }
        
//        uploader.upload(url: imageUrl, uploadPreset: "Sample_preset") { result, error in
//
//            let fetch = result?.url
//            print(fetch!)
//            self.fetchedUrl = URL(string: fetch!)
//            self.fetchedImageName = self.fetchedUrl.lastPathComponent
//            print("\(self.fetchedImageName)")
//
//        }
        //self.filtersCollectionView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: false)
    }
    @IBAction func nextBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatPostViewController") as! CreatPostViewController
            self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func editTextBtnAction(_ sender: Any) {
    }
    @IBAction func tagUserBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagUserViewController") as! TagUserViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func selectLocationBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func deleteBtnAction(_ sender: Any) {
        
       self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: - CollectionView Delegate Methods
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artFilters.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell:UICollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as UICollectionViewCell?)!
        
        let filterImageView = cell.viewWithTag(1) as! UIImageView
        let filterTitleLabel = cell.viewWithTag(2) as! UILabel
        
        let config = CLDConfiguration(cloudName: "venkicloudinary", apiKey: "s9Ls7hcfiRgcHiEaxgXVaelC89k")
        let cloudinary = CLDCloudinary(configuration: config)
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            DispatchQueue.main.async {
                
                filterImageView.cldSetImage(cloudinary.createUrl().setTransformation(CLDTransformation().setEffect(self.artFilters[indexPath.row])).generate("\(self.fetchedImageName)")!, cloudinary: cloudinary)
                var titleString = self.artFilters[indexPath.row]
                let range1 = titleString.characters.index(titleString.startIndex, offsetBy: 4)..<titleString.endIndex
                titleString = String(titleString[range1])
                filterTitleLabel.text = titleString
                
                //filterImageView.image = imageForButton
                
                filterImageView.layer.cornerRadius = 5
                filterImageView.layer.masksToBounds = false;
                filterImageView.clipsToBounds = true
                
            }
        }
    
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
//        let ciContext = CIContext(options: nil)
//        let originalImage = UIImage(named:"Aravind.jpg")
//        let coreImage = CIImage(image: originalImage!)
//        let filter = CIFilter(name: "\(CIFilterNames[indexPath.row])" )
//        filter!.setDefaults()
//        filter!.setValue(coreImage, forKey: kCIInputImageKey)
//        let filteredImageData = filter!.value(forKey: kCIOutputImageKey) as! CIImage
//        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
//        let imageForButton:UIImage = UIImage.init(cgImage: filteredImageRef!)
        
        let config = CLDConfiguration(cloudName: "venkicloudinary", apiKey: "s9Ls7hcfiRgcHiEaxgXVaelC89k")
        let cloudinary = CLDCloudinary(configuration: config)
        filterImageViewOutlet.cldSetImage(cloudinary.createUrl().setTransformation(CLDTransformation().setEffect(artFilters[indexPath.row])).generate("\(fetchedImageName)")!, cloudinary: cloudinary)
        
        //filterImageViewOutlet.image = imageForButton
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width / 4 - 10, height: collectionView.frame.size.height)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
