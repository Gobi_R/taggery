//
//  ForgotPasswordViewController.swift
//  Taggery
//
//  Created by Aravind on 24/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var newPasswordTxtFieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTxtfieldOutlet: SkyFloatingLabelTextField!
    @IBOutlet weak var doneBtnOutlet: UIButton!
    
    @IBOutlet weak var passwordValidImageView: UIImageView!
    @IBOutlet weak var confirmPasswordValidImageView: UIImageView!
    
    var isPasswordValid: Bool!
    var isConfirmPasswordValid: Bool!
    var phoneNumber = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Utilities.setBackgroungImage(self.view)
        
        doneBtnOutlet.layer.cornerRadius = 5;
        doneBtnOutlet.clipsToBounds = true;
        
        passwordValidImageView.isHidden = true
        confirmPasswordValidImageView.isHidden = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneBtnAction(_ sender: Any) {
        
        WebServiceHandler.sharedInstance.forgotPasswordApi(password: newPasswordTxtFieldOutlet.text!, mobileNumber: phoneNumber, viewController: self)
    }
    
    // MARK:- Text Field Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        
         if textField == newPasswordTxtFieldOutlet {
            
            if let password = textField.text, password.count >= 8 {
                passwordValidImageView.isHidden = false
                isPasswordValid = true
            }
            else
            {
                passwordValidImageView.isHidden = true
                isPasswordValid = false
            }
        }
        else if textField == confirmPasswordTxtfieldOutlet {
            
            if let confirmPassword = textField.text, confirmPassword.count >= 8 {
                
                if confirmPasswordTxtfieldOutlet.text == newPasswordTxtFieldOutlet.text
                {
                    confirmPasswordValidImageView.isHidden = false
                    isConfirmPasswordValid = true
                }
                else
                {
                    confirmPasswordValidImageView.isHidden = true
                    isConfirmPasswordValid = false
                }
            }
            else
            {
                confirmPasswordValidImageView.isHidden = true
                isConfirmPasswordValid = false
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
       if textField == newPasswordTxtFieldOutlet {
            
            if let password = textField.text, password.count >= 8 {
                passwordValidImageView.isHidden = false
                isPasswordValid = true
            }
            else
            {
                passwordValidImageView.isHidden = true
                isPasswordValid = false
            }
            
            confirmPasswordTxtfieldOutlet.becomeFirstResponder()
        }
        else if textField == confirmPasswordTxtfieldOutlet {
            
            if let confirmPassword = textField.text, confirmPassword.count >= 8 {
                
                if confirmPasswordTxtfieldOutlet.text == newPasswordTxtFieldOutlet.text
                {
                    confirmPasswordValidImageView.isHidden = false
                    isConfirmPasswordValid = true
                }
                else
                {
                    confirmPasswordValidImageView.isHidden = true
                    isConfirmPasswordValid = false
                }
            }
            else
            {
                confirmPasswordValidImageView.isHidden = true
                isConfirmPasswordValid = false
            }
            confirmPasswordTxtfieldOutlet.resignFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
         if textField == newPasswordTxtFieldOutlet
        {
            if string == " "{
                return false
            }
            else{
                passwordValidImageView.isHidden = true
                isPasswordValid = false
                return Utilities.userNameVadidation(string, CharactersInRange: range, textField: textField)
                
            }
        }
        else if textField == confirmPasswordTxtfieldOutlet
        {
            if string == " "{
                return false
            }
            else{
                confirmPasswordValidImageView.isHidden = true
                isConfirmPasswordValid = false
                return Utilities.userNameVadidation(string, CharactersInRange: range, textField: textField)
                
            }
        }
        else
        {
            return true
        }
    }
    
    func didReceiveforgotResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
            Utilities.showAlertView("Registration Failed", onView: self)
        }
    
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
