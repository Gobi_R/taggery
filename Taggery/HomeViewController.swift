//
//  HomeViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 06/03/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import Cloudinary

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var baseScrollViewOutlet: UIScrollView!
    @IBOutlet weak var profileImageViewOutlet: UIImageView!
    @IBOutlet weak var chatBtnOutlet: UIButton!
    @IBOutlet weak var searchBtnOutlet: UIButton!
    
    let post1 = ["Horse", "Cow", "Camel", "Sheep", "Goat","Horse", "Cow", "Camel"]
    let post2 = ["Horse", "Cow", "Camel", "Sheep", "Goat","Horse", "Cow"]
    let post3 = ["Horse", "Cow", "Camel", "Sheep", "Goat","Horse", "Cow", "Camel"]
    
    var totalPage = String()
    var curtrentPage = String()
    var homePostArray = [[String:Any]]()
    var locationPostArray = [[String:Any]]()
    var suggestionArray = [[String:Any]]()
    
    var isMypost: Bool!
    var isUnSeenpost: Bool!
    var issuggestion: Bool!
    var islocationPost: Bool!
    var isSeenpost: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.setBackgroungImage(self.view)
        
        self.loadPullToRefresh()
        
        tableViewOutlet.delegate = self
        tableViewOutlet.dataSource = self
        
        profileImageViewOutlet.layer.cornerRadius = 30;
        profileImageViewOutlet.clipsToBounds = true;
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        totalPage = "1"
        WebServiceHandler.sharedInstance.homeApiCall(userId: Constants.userId, lat: SharedVariables.sharedInstance.currentLatitude, lng: SharedVariables.sharedInstance.currentLatitude, page: "1", viewController: self)
        
    }
    
    func loadPullToRefresh() {
        
        let refreshControl: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(HomeViewController.handleRefresh(_:)),
                                     for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor.white
            
            return refreshControl
        }()
        self.tableViewOutlet.addSubview(refreshControl)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        totalPage = "1"
        
         isMypost = false
         isUnSeenpost = false
         issuggestion = false
         islocationPost = false
        isSeenpost = false
    
        WebServiceHandler.sharedInstance.homeApiCall(userId: Constants.userId, lat: SharedVariables.sharedInstance.currentLatitude, lng: SharedVariables.sharedInstance.currentLatitude, page: "1", viewController: self)
        
        //self.tableViewOutlet.reloadData()
        refreshControl.endRefreshing()
        print("PUll to refresh")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return homePostArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        var cell:UITableViewCell = UITableViewCell()
        
        let dict = homePostArray[indexPath.row]
        
            if issuggestion {
                
                let cell2:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "Cell2") as UITableViewCell?)!
                cell = cell2
                print(cell.subviews.last!)
                let contentView = cell.subviews.first
                print(contentView?.subviews.last!)
                
                let collectionView:UICollectionView = contentView?.subviews.last! as! UICollectionView
                collectionView.delegate = self
                collectionView.dataSource = self
                
                issuggestion = false
                return cell
            }
            
            if islocationPost
            {
                let cell3:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "Cell3") as UITableViewCell?)!
                cell = cell3
                
                let cell3PostImageView = cell.viewWithTag(1) as! UIImageView
                cell3PostImageView.layer.cornerRadius = 20
                cell3PostImageView.layer.masksToBounds = false;
                cell3PostImageView.clipsToBounds = true
                
                islocationPost = false
                return cell
            }
        
          if isSeenpost {

            let cell1:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "Cell4") as UITableViewCell?)!
            cell = cell1
            
            let postImageView = cell.viewWithTag(1) as! UIImageView
            let profileImageView = cell.viewWithTag(2) as! UIImageView
            let userName = cell.viewWithTag(3) as! UILabel
            
            let likeImageView = cell.viewWithTag(4) as! UIImageView
            let eyeImageView = cell.viewWithTag(6) as! UIImageView
            
            let likeCountLbl = cell.viewWithTag(5) as! UILabel
            let eyeCountLbl = cell.viewWithTag(7) as! UILabel
            let timeLbl = cell.viewWithTag(8) as! UILabel
            
            postImageView.layer.cornerRadius = 20
            postImageView.layer.masksToBounds = false;
            postImageView.clipsToBounds = true
            
            
            profileImageView.layer.cornerRadius = (profileImageView.frame.size.height) / 2
            profileImageView.layer.masksToBounds = false;
            profileImageView.clipsToBounds = true
            
            let postImageUrlStr = "\(dict["PostImage"]!)"
            let profileImageUrlStr = "\(dict["UserPicture"]!)"
            
            postImageView.sd_setImage(with: URL(string: postImageUrlStr), placeholderImage: UIImage(named: "placeHolder.jpg"))
            
            profileImageView.sd_setImage(with: URL(string: profileImageUrlStr), placeholderImage: UIImage(named: "placeHolder.jpg"))
            
            userName.text = "\(dict["UserName"]!)"
            likeCountLbl.text = "\(dict["LikeCount"]!)"
            eyeCountLbl.text = "\(dict["ViewCount"]!)"
            timeLbl.text = "\(dict["Feedtime"]!)"
            
            return cell
            
        }
        
        if isUnSeenpost {
            
            let cell1:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "Cell1") as UITableViewCell?)!
            cell = cell1
            
            let postImageView = cell.viewWithTag(1) as! UIImageView
            let profileImageView = cell.viewWithTag(2) as! UIImageView
            let userName = cell.viewWithTag(3) as! UILabel
            
            let likeImageView = cell.viewWithTag(4) as! UIImageView
            let eyeImageView = cell.viewWithTag(6) as! UIImageView
            
            let likeCountLbl = cell.viewWithTag(5) as! UILabel
            let eyeCountLbl = cell.viewWithTag(7) as! UILabel
            let timeLbl = cell.viewWithTag(8) as! UILabel
            
            postImageView.layer.cornerRadius = 20
            postImageView.layer.masksToBounds = false;
            postImageView.clipsToBounds = true
            
            
            profileImageView.layer.cornerRadius = (profileImageView.frame.size.height) / 2
            profileImageView.layer.masksToBounds = false;
            profileImageView.clipsToBounds = true
            
            let postImageUrlStr = "\(dict["PostImage"]!)"
            let profileImageUrlStr = "\(dict["UserPicture"]!)"
            
            postImageView.sd_setImage(with: URL(string: postImageUrlStr), placeholderImage: UIImage(named: "placeHolder.jpg"))
            
            profileImageView.sd_setImage(with: URL(string: profileImageUrlStr), placeholderImage: UIImage(named: "placeHolder.jpg"))
            
            userName.text = "\(dict["UserName"]!)"
            likeCountLbl.text = "\(dict["LikeCount"]!)"
            eyeCountLbl.text = "\(dict["ViewCount"]!)"
            timeLbl.text = "\(dict["Feedtime"]!)"
            
            return cell
        }
         if isMypost {
            
            let cell1:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "Cell1") as UITableViewCell?)!
            cell = cell1
            
            let postImageView = cell.viewWithTag(1) as! UIImageView
            let profileImageView = cell.viewWithTag(2) as! UIImageView
            let userName = cell.viewWithTag(3) as! UILabel
            
            let likeImageView = cell.viewWithTag(4) as! UIImageView
            let eyeImageView = cell.viewWithTag(6) as! UIImageView
            
            let likeCountLbl = cell.viewWithTag(5) as! UILabel
            let eyeCountLbl = cell.viewWithTag(7) as! UILabel
            let timeLbl = cell.viewWithTag(8) as! UILabel
            
            postImageView.layer.cornerRadius = 20
            postImageView.layer.masksToBounds = false;
            postImageView.clipsToBounds = true
            
            
            profileImageView.layer.cornerRadius = (profileImageView.frame.size.height) / 2
            profileImageView.layer.masksToBounds = false;
            profileImageView.clipsToBounds = true
            
            let postImageUrlStr = "\(dict["PostImage"]!)"
            let profileImageUrlStr = "\(dict["UserPicture"]!)"
            
            postImageView.sd_setImage(with: URL(string: postImageUrlStr), placeholderImage: UIImage(named: "placeHolder.jpg"))
            
            profileImageView.sd_setImage(with: URL(string: profileImageUrlStr), placeholderImage: UIImage(named: "placeHolder.jpg"))
            
            userName.text = "\(dict["UserName"]!)"
            likeCountLbl.text = "\(dict["LikeCount"]!)"
            eyeCountLbl.text = "\(dict["ViewCount"]!)"
            timeLbl.text = "\(dict["Feedtime"]!)"
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var cellHeight = Int()
        
        if indexPath.row >= 0 && indexPath.row < 4 {
           
          cellHeight = 150
        
        } else if indexPath.row == 4 {
            
            cellHeight = 125
            
        } else if indexPath.row > 4 {
           
            cellHeight = 150
        
        }
        
        return CGFloat(cellHeight)
    }
    
    
    // MARK: - CollectionView Delegate Methods
    

    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return post2.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as UICollectionViewCell!
        

        let postImageView = cell.viewWithTag(1) as! UIImageView
        let followBtn = cell.viewWithTag(2) as! UIButton
        
        postImageView.layer.cornerRadius = postImageView.frame.size.height / 2
        postImageView.layer.masksToBounds = false;
        postImageView.clipsToBounds = true
        
        followBtn.layer.cornerRadius = 5
        followBtn.layer.masksToBounds = false;
        followBtn.clipsToBounds = true
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width / 4 - 10, height: collectionView.frame.size.height)
    }
    
    // MARK: - Swipe Gesture Methods
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            
            let rightViewController = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController

            let transition = CATransition()
            transition.duration = 0.40
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
            navigationController?.view.layer.add(transition, forKey: kCATransition)
            navigationController?.pushViewController(rightViewController, animated: false)
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
            let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "leftViewController") as! leftViewController
            
            let transition = CATransition()
            transition.duration = 0.40
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            navigationController?.view.layer.add(transition, forKey: kCATransition)
            navigationController?.pushViewController(leftViewController, animated: false)
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView.isEqual(tableViewOutlet)
        {
            let offset:CGPoint = scrollView.contentOffset
            let bounds = scrollView.bounds
            let size:CGSize = scrollView.contentSize
            let inset = scrollView.contentInset
            let y: CGFloat = offset.y + bounds.size.height - inset.bottom
            let h: CGFloat = size.height - 200
            let reload_distance: CGFloat = 10
            
            if y > h + reload_distance
            {
                //totalPage = totalPage + 1
                let page = Int(curtrentPage)! + 1
                if totalPage == curtrentPage
                {
                    
                }
                else
                {
                WebServiceHandler.sharedInstance.homeApiCall(userId: Constants.userId, lat: SharedVariables.sharedInstance.currentLatitude, lng: SharedVariables.sharedInstance.currentLatitude, page: "\(page)", viewController: self)
                }
            }
        }
        
    }
    
    // MARK:- Api Response Methods
    
    func didReceiveHomeResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
    
             isMypost = responseDict["YourPosts"] != nil
             isUnSeenpost = responseDict["UnSeenPosts"] != nil
             issuggestion = responseDict["suggestion"] != nil
             islocationPost = responseDict["location"] != nil
             isSeenpost = responseDict["SeenPosts"] != nil
            
            
            print("isMypost",isMypost)
            print("isUnSeenpost",isUnSeenpost)
            print("issuggestion",issuggestion)
            print("islocationPost",islocationPost)
            print("isSeenpost",isSeenpost)
            
            var posts = [[String:Any]]()
        
            if isMypost
            {
                posts = responseDict["YourPosts"] as! [[String:Any]]
            }
            if isUnSeenpost
            {
                posts = responseDict["UnSeenPosts"] as! [[String:Any]]
            }
            if issuggestion
            {
                suggestionArray = responseDict["suggestion"] as! [[String:Any]]
            }
            if islocationPost
            {
                locationPostArray = responseDict["location"] as! [[String:Any]]
            }
            if isSeenpost
            {
                posts = responseDict["SeenPosts"] as! [[String:Any]]
            }
            
             totalPage = "\(responseDict["TotalPage"]!)"
             curtrentPage = "\(responseDict["CurrentPage"]!)"
            
            
            if curtrentPage == "1" {
                
                homePostArray = posts
                print("postArray:",homePostArray)
                
                
            }
            else
            {
                let productArray = posts
                homePostArray = homePostArray + productArray
                print("homeArray:",homePostArray)
                print("homeArrayCount:",homePostArray.count)
            }
            
            if homePostArray.count == 0
            {
                Utilities.showAlertView("No Results Found", onView: self)
            }
            
            tableViewOutlet.reloadData()
            
        }else{
            
        }
    }

}
