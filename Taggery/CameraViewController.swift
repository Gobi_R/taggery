//
//  CameraViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 07/03/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,AVCaptureVideoDataOutputSampleBufferDelegate {
    
    @IBOutlet var flashButton: UIButton!
    @IBOutlet var cameraFBbuttton: UIButton!
    @IBOutlet var showCameraView: UIView!
    @IBOutlet var captureButton: UIButton!
    let imagePicker = UIImagePickerController()
    var screenSize = UIScreen.main.bounds
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    // let WebServices = WebServicesHandler()
    
    var capturedImage = UIImage()
    var imageViewIdendity = String()
    // var imagesArray = [UIImage]()
    var imageUrl:URL!
    var cameraPlace = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.setBackgroungImage(self.view)
        
        self.captureButton.layer.cornerRadius = 10
        // self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.loadCamera()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        previewLayer?.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ImagePicker Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let tempImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let imageData  = UIImagePNGRepresentation(tempImage)

                    let filtersViewController = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
                    filtersViewController.imageUrl = self.imageUrl
                    filtersViewController.selectedImage = tempImage
                    filtersViewController.selectedImageData = imageData!
//                    let transition = CATransition()
//                    transition.duration = 0.40
//                    transition.type = kCATransitionPush
//                    transition.subtype = kCATransitionFromRight
//                    self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                    self.navigationController?.pushViewController(filtersViewController, animated: false)
                    
        
        self.capturedImage = tempImage;
        // self.loadImageForSellView()
    }
    
    // MARK: - Additional Methods
    
    func loadCamera()
    {
        captureSession = AVCaptureSession()
        let backCamera = AVCaptureDevice.default(for: AVMediaType.video)

        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            //Utility.showAlertView(String(describing: error), onView: self)
            print("error")
            input = nil
        }

        if error == nil && captureSession!.canAddInput(input) {
            captureSession!.addInput(input)

            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if captureSession!.canAddOutput(stillImageOutput!) {
                captureSession!.addOutput(stillImageOutput!)

                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                previewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                print(previewLayer!)

                showCameraView.layer.addSublayer(previewLayer!)

                // previewLayer!.frame = CGRect(x:0, y:0, width:showCameraView.frame.width, height:showCameraView.frame.height)

                print(previewLayer!.frame)

                let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
                swipeLeft.direction = .left
                showCameraView.addGestureRecognizer(swipeLeft)

                let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
                swipeRight.direction = .right
                showCameraView.addGestureRecognizer(swipeRight)

                let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
                swipeUp.direction = .up
                showCameraView.addGestureRecognizer(swipeUp)

                let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
                swipeDown.direction = .down
                showCameraView.addGestureRecognizer(swipeDown)

                captureSession!.startRunning()
            }
        }
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right Pick")
            
//            let rightViewController = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
//            let transition = CATransition()
//            transition.duration = 0.40
//            transition.type = kCATransitionPush
//            transition.subtype = kCATransitionFromLeft
//            navigationController?.view.layer.add(transition, forKey: kCATransition)
//            navigationController?.pushViewController(rightViewController, animated: false)
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left Orange")
            
            let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
            let transition = CATransition()
            transition.duration = 0.40
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            navigationController?.view.layer.add(transition, forKey: kCATransition)
            navigationController?.pushViewController(leftViewController, animated: false)
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up")
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.view.frame = CGRect(x: 0, y: 100, width: screenSize.size.width, height: screenSize.size.height-200)
            imagePicker.sourceType = .savedPhotosAlbum
            present(imagePicker, animated: true, completion: nil)
            
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
        }
    }
    
    // MARK: - Action Methods
    
    @IBAction func CaptureSelected(_ sender: AnyObject) {
        AudioServicesPlaySystemSound(1108)
        if let videoConnection = stillImageOutput!.connection(with: AVMediaType.video) {
            videoConnection.videoOrientation = AVCaptureVideoOrientation.portrait
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: {(sampleBuffer, error) in
                if (sampleBuffer != nil) {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
                    let dataProvider = CGDataProvider(data: imageData! as CFData)
                    let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                    self.capturedImage = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
                    
                    
                    let filtersViewController = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
                    filtersViewController.imageUrl = self.imageUrl
                    filtersViewController.selectedImage = self.capturedImage
                    filtersViewController.selectedImageData = imageData!
                    self.navigationController?.pushViewController(filtersViewController, animated: false)
                    

//                    let triggerTime = (Int64(NSEC_PER_SEC) * 1)
//                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: { () -> Void in
//                        //self.loadImageForSellView()
//                    })
                }
            })
        }
    }
    
    
    @IBAction func FlashSelected(_ sender: AnyObject) {
        
        // let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                    device?.torchMode = AVCaptureDevice.TorchMode.off
                } else {
                    do {
                        try device?.setTorchModeOn(level: 1.0)
                    } catch {
                        //Utility.showAlertView(String(describing: error), onView: self)
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                //Utility.showAlertView(String(describing: error), onView: self)
                print(error)
            }
        }
    }
    
    @IBAction func CameraFBSelected(_ sender: AnyObject) {
        if let session = captureSession {
            //Indicate that some changes will be made to the session
            session.beginConfiguration()
            
            //Remove existing input
            let currentCameraInput:AVCaptureInput = session.inputs.first as! AVCaptureInput
            session.removeInput(currentCameraInput)
            
            //Get new input
            var newCamera:AVCaptureDevice! = nil
            if let input = currentCameraInput as? AVCaptureDeviceInput {
                if (input.device.position == .back)
                {
                    newCamera = cameraWithPosition(.front)
                }
                else
                {
                    newCamera = cameraWithPosition(.back)
                }
            }
            
            //Add input to session
            var err: NSError?
            var newVideoInput: AVCaptureDeviceInput!
            do {
                newVideoInput = try AVCaptureDeviceInput(device: newCamera)
            } catch let err1 as NSError {
                err = err1
                newVideoInput = nil
            }
            
            if(newVideoInput == nil || err != nil)
            {
                //print("Error creating capture device input: \(err!.localizedDescription)")
            }
            else
            {
                session.addInput(newVideoInput)
            }
            
            //Commit all the configuration changes at once
            session.commitConfiguration()
        }
    }
    
    // MARK: - Camera Methods
    
    func cameraWithPosition(_ position: AVCaptureDevice.Position) -> AVCaptureDevice?
    {
        let devices = AVCaptureDevice.devices(for: AVMediaType.video)
        for device in devices {
            let device = device as! AVCaptureDevice
            if device.position == position {
                return device
            }
        }
        
        return nil
    }
    
    
}

