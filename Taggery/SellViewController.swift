//
//  SellViewController.swift
//  Karungguni
//  Created purpose:Sellscreen will display the pickup images from camera or device images
//  Created by Aravind Kumar on 9/13/16.
//  Copyright © 2016 Palm Pte Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class SellViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    @IBOutlet var topMenuView: UIView!
    @IBOutlet var flashButton: UIButton!
    @IBOutlet var cameraFBbuttton: UIButton!
    @IBOutlet var showCameraView: UIView!
    @IBOutlet var bottomMenuView: UIView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var galaryButton: UIButton!
    @IBOutlet var captureButton: UIButton!
    let imagePicker = UIImagePickerController()
    var screenSize = UIScreen.main.bounds
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
   // let WebServices = WebServicesHandler()
    
    var capturedImage = UIImage()
    var imageViewIdendity = String()
   // var imagesArray = [UIImage]()
    var cameraPlace = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.captureButton.layer.cornerRadius = 10
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.loadCamera()
    }
    
    /////// image Pickerview delegate method ///////////
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let tempImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.capturedImage = tempImage;
        self.loadImageForSellView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        previewLayer?.frame = showCameraView.bounds
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///////// load custom camera /////////
    
    func loadCamera()
    {
        captureSession = AVCaptureSession()
        //captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
        
      //  let backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
        
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: backCamera!)
        } catch let error1 as NSError {
            error = error1
            //Utility.showAlertView(String(describing: error), onView: self)
            print("error")
            input = nil
        }
        
        if error == nil && captureSession!.canAddInput(input) {
            captureSession!.addInput(input)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if captureSession!.canAddOutput(stillImageOutput!) {
                captureSession!.addOutput(stillImageOutput!)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                previewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                print(previewLayer)
                //self.previewLayer?.frame = self.view.bounds
                showCameraView.layer.addSublayer(previewLayer!)
                print(showCameraView.layer.frame)
                
                captureSession!.startRunning()
            }
        }
    } 
    ///////// get flash selected /////////
   
    @IBAction func FlashSelected(_ sender: AnyObject) {
        
       // let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                    device?.torchMode = AVCaptureDevice.TorchMode.off
                } else {
                    do {
                        try device?.setTorchModeOn(level: 1.0)
                    } catch {
                        //Utility.showAlertView(String(describing: error), onView: self)
                        print(error)
                    }
                }
                device?.unlockForConfiguration()
            } catch {
                //Utility.showAlertView(String(describing: error), onView: self)
                print(error)
            }
        }
    }
    
    ///////// get camera front and back option /////////
    
    @IBAction func CameraFBSelected(_ sender: AnyObject) {
        if let session = captureSession {
            //Indicate that some changes will be made to the session
            session.beginConfiguration()
            
            //Remove existing input
            let currentCameraInput:AVCaptureInput = session.inputs.first as! AVCaptureInput
            session.removeInput(currentCameraInput)
            
            //Get new input
            var newCamera:AVCaptureDevice! = nil
            if let input = currentCameraInput as? AVCaptureDeviceInput {
                if (input.device.position == .back)
                {
                    newCamera = cameraWithPosition(.front)
                }
                else
                {
                    newCamera = cameraWithPosition(.back)
                }
            }
            
            //Add input to session
            var err: NSError?
            var newVideoInput: AVCaptureDeviceInput!
            do {
                newVideoInput = try AVCaptureDeviceInput(device: newCamera)
            } catch let err1 as NSError {
                err = err1
                newVideoInput = nil
            }
            
            if(newVideoInput == nil || err != nil)
            {
                //print("Error creating capture device input: \(err!.localizedDescription)")
            }
            else
            {
                session.addInput(newVideoInput)
            }
            
            //Commit all the configuration changes at once
            session.commitConfiguration()
        }
    }
    
    /////////// to idendify camera position fornt or back ////////////
    
    func cameraWithPosition(_ position: AVCaptureDevice.Position) -> AVCaptureDevice?
    {
        let devices = AVCaptureDevice.devices(for: AVMediaType.video)
        for device in devices {
            let device = device as! AVCaptureDevice
            if device.position == position {
                return device
            }
        }
        
        return nil
    }

    ///////// cancel button selected /////////
    
    @IBAction func CancelSelected(_ sender: AnyObject) {
//        if SharedVariables.sharedInstance.isProductEdited == true
//        {
//            _ = self.navigationController?.popViewController(animated: true)
//        }
//        else if SharedVariables.sharedInstance.loadItemImagesArray.isEmpty
//        {
//            SharedVariables.sharedInstance.loadItemImagesArray.removeAll()
//            let tabBarController = SharedVariables.sharedInstance.myTabBarController
//            // let tabBarController: UITabBarController = (tabBarController as? UITabBarController)!
//            tabBarController.selectedIndex = 0
//
//        }
//        else
//        {
//            let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainSell") as! MainSellViewController
//            let navController = UINavigationController(rootViewController: vc)
//            self.present(navController, animated:true, completion: nil)
//
//        }
        
        
    }
    
    ///////// load mobile galary /////////
    
    @IBAction func GalarySelected(_ sender: AnyObject) {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.view.frame = CGRect(x: 0, y: 100, width: screenSize.size.width, height: screenSize.size.height-200)
        imagePicker.sourceType = .savedPhotosAlbum
        present(imagePicker, animated: true, completion: nil)
    }
    
    ///////// photo clicked /////////

    @IBAction func CaptureSelected(_ sender: AnyObject) {
        //AudioServicesPlaySystemSound(1108)
        if let videoConnection = stillImageOutput!.connection(with: AVMediaType.video) {
            videoConnection.videoOrientation = AVCaptureVideoOrientation.portrait
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: {(sampleBuffer, error) in
                if (sampleBuffer != nil) {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer!)
                    let dataProvider = CGDataProvider(data: imageData as! CFData)
                    let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                     self.capturedImage = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
                    
                    let triggerTime = (Int64(NSEC_PER_SEC) * 1)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: { () -> Void in
                        self.loadImageForSellView()
                 })
                }
             })
           }
        }
    ///////// load sell images /////////
    
func loadImageForSellView()
{
//    if SharedVariables.sharedInstance.isProductEdited == true
//    {
//                SharedVariables.sharedInstance.loadEditedItemImagesArray[SharedVariables.sharedInstance.cameraPlace] = self.capturedImage
//                 _ = self.navigationController?.popViewController(animated: true)
//    }
//    else
//    {
//
//    if SharedVariables.sharedInstance.loadItemImagesArray.isEmpty
//    {
//        if SharedVariables.sharedInstance.firstNavigation == "" {
//
//            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MainSell") as! MainSellViewController!
//            let navController = UINavigationController(rootViewController: vc!)
//            // print(vc)
//            SharedVariables.sharedInstance.loadItemImagesArray[1] = self.capturedImage
//            vc?.itemImage = self.capturedImage
//            vc?.idendifierString = imageViewIdendity
//            SharedVariables.sharedInstance.firstNavigation = "navigation"
//            //print(self.navigationController)
//            self.present(navController, animated:true, completion: nil)
//
//        }
//        else{
//
//            let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainSell") as! MainSellViewController
//            let navController = UINavigationController(rootViewController: vc)
//            // print(vc)
//            // SharedVariables.sharedInstance.loadItemImagesArray.append(self.capturedImage)
//            SharedVariables.sharedInstance.loadItemImagesArray[SharedVariables.sharedInstance.cameraPlace] = self.capturedImage
//            vc.itemImage = self.capturedImage
//            vc.idendifierString = imageViewIdendity
//            // print(self.navigationController)
//            self.present(navController, animated:true, completion: nil)
//
//        }
//
//    }
//    else
//    {
//    if SharedVariables.sharedInstance.firstNavigation == "" {
//
//        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MainSell") as! MainSellViewController!
//        let navController = UINavigationController(rootViewController: vc!)
//       // print(vc)
//        SharedVariables.sharedInstance.loadItemImagesArray[1] = self.capturedImage
//        vc?.itemImage = self.capturedImage
//        vc?.idendifierString = imageViewIdendity
//        SharedVariables.sharedInstance.firstNavigation = "navigation"
//        //print(self.navigationController)
//        self.present(navController, animated:true, completion: nil)
//
//    }
//    else{
//
//        let vc = self.storyboard!.instantiateViewController(withIdentifier: "MainSell") as! MainSellViewController
//        let navController = UINavigationController(rootViewController: vc)
//       // print(vc)
//        // SharedVariables.sharedInstance.loadItemImagesArray.append(self.capturedImage)
//        SharedVariables.sharedInstance.loadItemImagesArray[SharedVariables.sharedInstance.cameraPlace] = self.capturedImage
//        vc.itemImage = self.capturedImage
//        vc.idendifierString = imageViewIdendity
//       // print(self.navigationController)
//        self.present(navController, animated:true, completion: nil)
//
//      }
//    }
//    }
}
}
