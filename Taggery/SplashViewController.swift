//
//  SplashViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 11/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import CoreLocation

class SplashViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var spashImageViewOutlet: UIImageView!
    
    let locationManager = CLLocationManager()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.determineMyCurrentLocation()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BG.png")!)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
//            self.navigationController?.pushViewController(vc, animated: true)
        }

        WebServiceHandler.sharedInstance.getCountry(viewController: self)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func determineMyCurrentLocation() {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 50
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        SharedVariables.sharedInstance.currentLatitude = "\(userLocation.coordinate.latitude)"
        SharedVariables.sharedInstance.currentLongitude = "\(userLocation.coordinate.longitude)"
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    func didReceiveCountryList(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            SharedVariables.sharedInstance.countryListArray = responseDict["country"] as! [[String : Any]] as NSArray
            
            let isLogin:Bool = UserDefaults.standard.bool(forKey: "isLogin")

            
            if isLogin
            {
                let userDetails = UserDefaults.standard.object(forKey: "userDetail")! as! [String:Any]
                Constants.userId = "\(userDetails["id"]!)"
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
            
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
