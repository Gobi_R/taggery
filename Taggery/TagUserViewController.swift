//
//  TagUserViewController.swift
//  Taggery
//
//  Created by Aravind on 27/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class TagUserViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var serachTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if(searchActive) {
//            return filtered.count
//        }
        return SharedVariables.sharedInstance.countryListArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableViewOutlet.dequeueReusableCell(withIdentifier: "Cell");
        
        
        let namelabel:UILabel = cell!.viewWithTag(2) as! UILabel
        let imageView:UIImageView = cell!.viewWithTag(1) as! UIImageView
//        let phonrCodelabel:UILabel = cell!.viewWithTag(3) as! UILabel
//        var countryDict: [String:Any]
        
//        if(searchActive){
//            countryDict = filtered[indexPath.row];
//        } else {
        let countryDict = SharedVariables.sharedInstance.countryListArray[indexPath.row] as! [String:Any]
//        }
        namelabel.text = countryDict["nicename"] as? String
       // phonrCodelabel.text = "+\(String(describing: countryDict["phonecode"]!))"
        let url = "\(String(describing: countryDict["flag"]!))"

        imageView.layer.cornerRadius = imageView.frame.size.height / 2;
        imageView.clipsToBounds = true;

        imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeHolder.jpg"))
        
        
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        if(searchActive){
//            SharedVariables.sharedInstance.selectedCountryDict = filtered[indexPath.row];
//
//        } else {
//            SharedVariables.sharedInstance.selectedCountryDict = SharedVariables.sharedInstance.countryListArray[indexPath.row] as! [String : Any]
//
//        }
//        print(SharedVariables.sharedInstance.selectedCountryDict)
        self.dismiss(animated: true, completion: {
            //self.searchActive = false
        })
        
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 60
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
    
        return true
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
