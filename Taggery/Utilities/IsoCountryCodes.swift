//
//  IsoCountryCodes.swift
//  IsoCountryCodes
//
//  Created by Karthikeyan A. on 12/01/18.
//  Copyright © 2018 DCI. All rights reserved.
//

class IsoCountryCodes {
    
    class func find( key:String ) -> IsoCountryInfo {
        var country = IsoCountries.allCountries.filter(  { $0.alpha2 == key.uppercased() || $0.alpha3 == key.uppercased() || $0.numeric == key } )
        return country[0]
    }
    
    class func searchByName( name:String ) -> IsoCountryInfo {
        var country = IsoCountries.allCountries.filter( { $0.name == name } )
        return (!country.isEmpty) ? country[0] : IsoCountryInfo(name: "", numeric: "", alpha2: "", alpha3: "", calling: "", currency: "", continent: "")
    }
    
    
    class func searchByCountry( name:String ) -> IsoCountryInfo {
        var country = IsoCountries.allCountries.filter( { $0.alpha2 == name } )
        return (!country.isEmpty) ? country[0] : IsoCountryInfo(name: "", numeric: "", alpha2: "", alpha3: "", calling: "", currency: "", continent: "")
    }
    class func searchByAlpha2( currency:String ) -> String {
        var country = IsoCountries.allCountries.filter( { $0.name == currency } )
        if !country.isEmpty{
            return country[0].alpha2
        }
        return ""
    }
    
    class func searchByCode( name:String ) -> IsoCountryInfo {
        var country = IsoCountries.allCountries.filter( { $0.alpha2 == name } )
        
        return (!country.isEmpty) ? country[0] : IsoCountryInfo(name: "", numeric: "", alpha2: "", alpha3: "", calling: "", currency: "", continent: "")
    }
  //  -> IsoCountryInfo
    class func searchByCurrency( currency: String ) -> [IsoCountryInfo] {
       return  IsoCountries.allCountries.filter { $0.currency.contains(currency.uppercased())}
    }
    class func searchByCurrencyCountry( name:String ) -> [IsoCountryInfo] {
        return IsoCountries.allCountries.filter{ $0.name.contains(name.uppercased())}
    }
    
    class func searchByCallingCode( calllingCode:String ) -> IsoCountryInfo {
        var country = IsoCountries.allCountries.filter( { $0.calling == calllingCode } )
        return country[0]
    }
}
