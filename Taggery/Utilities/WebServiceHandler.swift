//
//  WebServiceHandler.swift
//  MagmaChamberCalendar
//
//  Created by Balajibabu S.G. on 28/10/17.
//  Copyright © 2017 Balajibabu S.G. All rights reserved.
//

import UIKit
import Alamofire

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
}

class WebServiceHandler: NSObject {
    
    static let sharedInstance = WebServiceHandler()
    
    let headerBase64Data = (Constants.k_Webservice_Header).data(using: String.Encoding.utf8)!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    
    // MARK:- Register Api Call
    
    func registration(FirstName:String, LastName:String, UserName:String, PhoneNumber:String, Gender:String, RelationShipStatus:String, Password:String, FCMKey:String, DeviceType:String, viewController:UIViewController){
        
        let parameters = ["FirstName": FirstName, "LastName": LastName, "UserName": UserName, "PhoneNumber": PhoneNumber, "Gender": Gender, "RelationShipStatus": RelationShipStatus, "Password": Password, "FCMKey": FCMKey, "DeviceType": DeviceType]
        
        let header = ["WSH" : headerBase64Data]
        
        let url = "\(Constants.k_Webservice_URL)registration"

        Alamofire.request(url, method: .post, parameters: parameters, headers: header)
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if viewController.isKind(of: RegisterationViewController.self) {
                    let regVC = viewController as! RegisterationViewController
                    UIApplication.shared.endIgnoringInteractionEvents()

                }
                switch response.result {
                case .success:

                    if response.result.value != nil {
                        print("SUCCESS:\(response.result.value!)")
                        if (response.result.value as! [String:Any])["Status"]! as! String == "Success" {

                            if viewController.isKind(of: RegisterationViewController.self) {
                                let VC = viewController as! RegisterationViewController
                                VC.didReceiveRegistrationResponse(responseDict: (response.result.value as AnyObject) as! [String : Any])
                            }
                        }
                        else {
                            if ((response.result.value as! [String:Any])["Message"] as? String) != nil {

                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Message"]! as! String), onView: viewController)
                            }
                            else {
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Response"]! as! String), onView: viewController)
                            }
                        }
                    }
                    break
                case .failure(let error):
                    let error:String = error.localizedDescription
                    print("ERROR:\(error)")
                    Utilities.showAlertView(error, onView: viewController)
                    break
                }
    }
    }
    
    func getCountry(viewController:UIViewController){
        
        let headerBase64Data = (Constants.k_Webservice_Header).data(using: String.Encoding.utf8)!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let header = ["WSH" : headerBase64Data]
        let url = "\(Constants.k_Webservice_URL)country"
        
        print(header)
        print(url)
        
        Alamofire.request(url, method: .get, parameters:nil, headers: header)
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
//                if viewController.isKind(of: RegisterationViewController.self) {
//                    let regVC = viewController as! RegisterationViewController
//                    UIApplication.shared.endIgnoringInteractionEvents()
//
//                }
                //UIApplication.shared.endIgnoringInteractionEvents()
                switch response.result {
                case .success:
                    
                    if response.result.value != nil {
                        print("SUCCESS:\(response.result.value!)")
                        if (response.result.value as! [String:Any])["Status"]! as! String == "Success" {
                            
                                                        if viewController.isKind(of: SplashViewController.self) {
                                                            let vc = viewController as! SplashViewController
                                                            vc.didReceiveCountryList(responseDict: response.result.value as! [String : Any])
                                                        }
                        }
                        else {
                            if ((response.result.value as! [String:Any])["Message"] as? String) != nil {
                                
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Message"]! as! String), onView: viewController)
                            }
                            else {
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Response"]! as! String), onView: viewController)
                            }
                        }
                    }
                    break
                case .failure(let error):
                    let error:String = error.localizedDescription
                    print("ERROR:\(error)")
                    Utilities.showAlertView(error, onView: viewController)
                    break
                }
        }
        
    }
    
    func userNameCheck(name:String, viewController:UIViewController){
    
        let parameters = ["UserName": name]
        
        let header = ["WSH" : headerBase64Data]
        
        let url = "\(Constants.k_Webservice_URL)checkusername"
         
        Alamofire.request(url, method: .post, parameters: parameters, headers: header)
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if viewController.isKind(of: RegisterationViewController.self) {
                    let regVC = viewController as! RegisterationViewController
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                }
                switch response.result {
                case .success:
                    
                    if response.result.value != nil {
                        print("SUCCESS:\(response.result.value!)")
                        if (response.result.value as! [String:Any])["StatusCode"]! as! Int == 200 {
                            
                        if viewController.isKind(of: RegisterationViewController.self) {
                                                            let VC = viewController as! RegisterationViewController
                            VC.didReceiveUserNameCheck(responseDict: (response.result.value as AnyObject) as! [String : Any])
                                                        }
                        }
                        else {
                            if ((response.result.value as! [String:Any])["Message"] as? String) != nil {
                                
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Message"]! as! String), onView: viewController)
                            }
                            else {
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Response"]! as! String), onView: viewController)
                            }
                        }
                    }
                    break
                case .failure(let error):
                    let error:String = error.localizedDescription
                    print("ERROR:\(error)")
                    Utilities.showAlertView(error, onView: viewController)
                    break
                }
        }
    }
    
    func phoneNumberCheck(phoneNumber:String, viewController:UIViewController){
        
        let parameters = ["UserMobileNumber": phoneNumber]
        let header = ["WSH" : headerBase64Data]
        let url = "\(Constants.k_Webservice_URL)verifyphnnum"
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: header)
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if viewController.isKind(of: RegisterationViewController.self) {
                    let regVC = viewController as! RegisterationViewController
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                }
                switch response.result {
                case .success:
                    
                    if response.result.value != nil {
                        print("SUCCESS:\(response.result.value!)")
                        if (response.result.value as! [String:Any])["StatusCode"]! as! Int == 200 {
                            
                            if viewController.isKind(of: LoginViewController.self) {
                                let VC = viewController as! LoginViewController
                                VC.didReceivePhoneNumberCheck(responseDict: (response.result.value as AnyObject) as! [String : Any])
                            }
                        }
                        else {
                            if ((response.result.value as! [String:Any])["Message"] as? String) != nil {
                                
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Message"]! as! String), onView: viewController)
                            }
                            else {
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Response"]! as! String), onView: viewController)
                            }
                        }
                    }
                    break
                case .failure(let error):
                    let error:String = error.localizedDescription
                    print("ERROR:\(error)")
                    Utilities.showAlertView(error, onView: viewController)
                    break
                }
        }
    }
    
    func forgotPasswordApi(password:String, mobileNumber:String, viewController:UIViewController){
        
        let parameters = ["UserMobileNumber": mobileNumber, "UserPassword": password]
        let header = ["WSH" : headerBase64Data]
        let url = "\(Constants.k_Webservice_URL)forgetpassword"
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: header)
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if viewController.isKind(of: ForgotPasswordViewController.self) {
                    let regVC = viewController as! ForgotPasswordViewController
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                }
                switch response.result {
                case .success:
                    
                    if response.result.value != nil {
                        print("SUCCESS:\(response.result.value!)")
                        if (response.result.value as! [String:Any])["StatusCode"]! as! Int == 200 {
                            
                            if viewController.isKind(of: ForgotPasswordViewController.self) {
                                let vc = viewController as! ForgotPasswordViewController
                                vc.didReceiveforgotResponse(responseDict: (response.result.value as AnyObject) as! [String : Any])
                            }
                        }
                        else {
                            if ((response.result.value as! [String:Any])["Message"] as? String) != nil {
                                
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Message"]! as! String), onView: viewController)
                            }
                            else {
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Response"]! as! String), onView: viewController)
                            }
                        }
                    }
                    break
                case .failure(let error):
                    let error:String = error.localizedDescription
                    print("ERROR:\(error)")
                    Utilities.showAlertView(error, onView: viewController)
                    break
                }
        }
    }
    
    func logInApiCall(password:String, mobileNumber:String, viewController:UIViewController){
        
        let parameters = ["PhoneNumber": mobileNumber, "Password": password]
        let header = ["WSH" : headerBase64Data]
        let url = "\(Constants.k_Webservice_URL)login"
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: header)
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if viewController.isKind(of: LoginViewController.self) {
                    let regVC = viewController as! LoginViewController
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                }
                switch response.result {
                case .success:
                    
                    if response.result.value != nil {
                        print("SUCCESS:\(response.result.value!)")
                        if (response.result.value as! [String:Any])["StatusCode"]! as! Int == 200 {
                            
                            if viewController.isKind(of: LoginViewController.self) {
                                let vc = viewController as! LoginViewController
                                vc.didReceiveLogInResponse(responseDict: (response.result.value as AnyObject) as! [String : Any])
                            }
                        }
                        else {
                            if ((response.result.value as! [String:Any])["Message"] as? String) != nil {
                                
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Message"]! as! String), onView: viewController)
                            }
                            else {
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Response"]! as! String), onView: viewController)
                            }
                        }
                    }
                    break
                case .failure(let error):
                    let error:String = error.localizedDescription
                    print("ERROR:\(error)")
                    Utilities.showAlertView(error, onView: viewController)
                    break
                }
        }
    }
    
    func homeApiCall(userId:String, lat:String, lng:String, page:String, viewController:UIViewController){
        
        var parameters = ["user_id": userId, "page": page, "lat": lat, "lng": lng]
    
        let header = ["WSH" : headerBase64Data]
        let url = "\(Constants.k_Webservice_URL)homepage"
        
//        if (page == "1")
//        {
            parameters = ["user_id": userId, "page": page, "lat": lat, "lng": lng]
            //print(urlString)
//        }
//        else
//        {
//            if(SharedVariables.sharedInstance.homePage == page)
//            {
//                return
//            }
//            else{
//                SharedVariables.sharedInstance.homePage = String(Int(SharedVariables.sharedInstance.homePage)! +  1)
//                
//                parameters = ["user_id": userId, "page": SharedVariables.sharedInstance.homePage, "lat": lat, "lng": lng]
//            }
//        }
        
        print(parameters)
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: header)
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if viewController.isKind(of: HomeViewController.self) {
                    let regVC = viewController as! HomeViewController
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                }
                switch response.result {
                case .success:
                    
                    if response.result.value != nil {
                        print("SUCCESS:\(response.result.value!)")
                        if (response.result.value as! [String:Any])["StatusCode"]! as! Int == 200 {
                            
                            if viewController.isKind(of: HomeViewController.self) {
                                let vc = viewController as! HomeViewController
                                vc.didReceiveHomeResponse(responseDict: (response.result.value as AnyObject) as! [String : Any])
                            }
                        }
                        else {
                            if ((response.result.value as! [String:Any])["Message"] as? String) != nil {
                                
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Message"]! as! String), onView: viewController)
                            }
                            else {
                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Response"]! as! String), onView: viewController)
                            }
                        }
                    }
                    break
                case .failure(let error):
                    let error:String = error.localizedDescription
                    print("ERROR:\(error)")
                    Utilities.showAlertView(error, onView: viewController)
                    break
                }
        }
    }
    
    // MARK:- OTP Api Call
    
    func sendOtp(viewController:UIViewController,authkey:String,message:String,sender:String,mobile:String)
    {
        let urlString = "http://control.msg91.com/api/sendotp.php"
        
        //create the url with URL
        let url = URL(string: urlString)! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST   ["WS" : ParametersBase64]
        
        //  let postString = "useremail=\(useremail)&password=\(useremail)"
        
       // let postString = "authkey=\(Constants.k_Mgs91_AuthKey)&message=\(message)&sender=\(sender)&mobile=\(mobile)"
        
         let postString = "authkey=\(Constants.k_Mgs91_AuthKey)&message=Your verification code for Magma Chamber Calendar is ##OTP##.&sender=OTPSMS&mobile=\(mobile)&otp_expiry=2"
        
        request.httpBody = postString.data(using: .utf8)
        
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                print(data)
                let backToString = String(data: data, encoding: String.Encoding.utf8) as String!
                //                let endIndex = backToString?.index((backToString?.endIndex)!, offsetBy: -4)
                //                let truncated = backToString?.substring(to: endIndex!)
                print(backToString!)
                
                
                if let data1 = backToString!.data(using: .utf8) {
                    
                    if let json = try JSONSerialization.jsonObject(with: data1, options: .mutableContainers) as? [String: Any] {
                        print(json)
                        
                        DispatchQueue.main.async {

                        if viewController.isKind(of: LoginViewController.self) {
                            let vc = viewController as! LoginViewController
                            vc.didReceiveOtpResponse(responseDict: json)
                        }
                        }
                        
                       // profileviewcontroller.didReceiveOtp(json as AnyObject)
                        
                        
                    }
                }
                //create json object from data
                
            } catch let error {
                print(error.localizedDescription)
                // Utilities.showAlertView(error.localizedDescription, onView: mainViewController)
            }
        })
        task.resume()
        
        
        
    }
    func otpVerify(viewController:UIViewController,authkey:String,mobile:String,otp:String)
    {
        let urlString = "https://control.msg91.com/api/verifyRequestOTP.php"
        
        //create the url with URL
        let url = URL(string: urlString)! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST   ["WS" : ParametersBase64]
        
        //  let postString = "useremail=\(useremail)&password=\(useremail)"
        
       // let postString = "authkey=\(Constants.k_Mgs91_AuthKey)&mobile=\(mobile)&otp=\(otp)"
        
        let postString = "authkey=\(Constants.k_Mgs91_AuthKey)&mobile=\(mobile)&otp=\(otp)&message=Your verification code for Magma Chamber Calendar is \(otp)&otp_expiry=2"
        print("OTP Verification Param: ",postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                print(data)
                let backToString = String(data: data, encoding: String.Encoding.utf8) as String!
                //                let endIndex = backToString?.index((backToString?.endIndex)!, offsetBy: -4)
                //                let truncated = backToString?.substring(to: endIndex!)
                print(backToString!)
                
                
                if let data1 = backToString!.data(using: .utf8) {
                    
                    if let json = try JSONSerialization.jsonObject(with: data1, options: .mutableContainers) as? [String: Any] {
                        print(json)
                        
                        DispatchQueue.main.async {
                        if viewController.isKind(of: OtpVerificationViewController.self) {
                            let vc = viewController as! OtpVerificationViewController
                            vc.didReceiveOtpVerificationResponse(responseDict: json)
                        }
                        }
                       // profileviewcontroller.didReceiveVerifyOtp(json as AnyObject)
                        
                        
                    }
                }
                //create json object from data
                
            } catch let error {
                print(error.localizedDescription)
                // Utilities.showAlertView(error.localizedDescription, onView: mainViewController)
            }
        })
        task.resume()
        
        
        
    }
    
    func resendOtp(viewController:UIViewController,authkey:String,mobile:String)
    {
        let urlString = "http://control.msg91.com/api/retryotp.php"
        
        //create the url with URL
        let url = URL(string: urlString)! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST   ["WS" : ParametersBase64]
        
        //  let postString = "useremail=\(useremail)&password=\(useremail)"
        
       // let postString = "authkey=\(Constants.k_Mgs91_AuthKey)&mobile=\(mobile)"
        
        let postString = "authkey=\(Constants.k_Mgs91_AuthKey)&mobile=\(mobile)"
        
        request.httpBody = postString.data(using: .utf8)
        
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                print(data)
                let backToString = String(data: data, encoding: String.Encoding.utf8) as String!
                //                let endIndex = backToString?.index((backToString?.endIndex)!, offsetBy: -4)
                //                let truncated = backToString?.substring(to: endIndex!)
                print(backToString!)
                
                
                if let data1 = backToString!.data(using: .utf8) {
                    
                    if let json = try JSONSerialization.jsonObject(with: data1, options: .mutableContainers) as? [String: Any] {
                        print(json)
                        
                        DispatchQueue.main.async {
                        if viewController.isKind(of: OtpVerificationViewController.self) {
                            let vc = viewController as! OtpVerificationViewController
                            vc.didReceiveReSendOtpResponse(responseDict: json)
                        }
                        }
                      //  profileviewcontroller.didReceiveResendOtp(json as AnyObject)
                        
                        
                    }
                }
                //create json object from data
                
            } catch let error {
                print(error.localizedDescription)
                // Utilities.showAlertView(error.localizedDescription, onView: mainViewController)
            }
        })
        task.resume()
        
        
        
    }
    
//    func login(loginParamDict:[String:Any], viewController:UIViewController){
//
//        if UserDefaults.standard.value(forKey: "FCMToken") == nil {
//            UserDefaults.standard.set("SimulatorCheck", forKey: "FCMToken")
//        }
//
//        let parameterString = "email:\(loginParamDict["Email"]!)|password:\(loginParamDict["Password"]!)|fcm_key:\(UserDefaults.standard.value(forKey: "FCMToken")!)|Function:Login"
//
//        let parametersAndHeader = getParametersAndHeaders(paramString: parameterString)
//
//        Alamofire.request(Constants.k_Webservice_URL, method: .post, parameters: parametersAndHeader.parameters, headers: parametersAndHeader.header)
//            .validate { request, response, data in
//                return .success
//            }
//            .responseJSON { response in
//                if viewController.isKind(of: LoginViewController.self) {
//                    let loginVC = viewController as! LoginViewController
//                    UIApplication.shared.endIgnoringInteractionEvents()
//                    loginVC.navigationCustom()
//                }
//                switch response.result {
//                case .success:
//
//                    if response.result.value != nil {
//                        print("SUCCESS:\(response.result.value!)")
//                        if (response.result.value as! [String:Any])["Status"]! as! String == "Success" {
//                            if viewController.isKind(of: LoginViewController.self) {
//                                let loginVC = viewController as! LoginViewController
//                                loginVC.didReceiveLoginResponse(response.result.value as AnyObject)
//                            }
//                        }
//                        else {
//                            if ((response.result.value as! [String:Any])["Message"] as? String) != nil {
//
//                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Message"]! as! String), onView: viewController)
//                            }
//                            else {
//                                Utilities.showAlertView( ((response.result.value as! [String:Any])["Response"]! as! String), onView: viewController)
//                            }
//
//                        }
//                    }
//                    break
//                case .failure(let error):
//
//                    let error:String = error.localizedDescription
//                    print("ERROR:\(error)")
//                    Utilities.showAlertView(error, onView: viewController)
//                    break
//                }
//        }
//
//
//
//    }
    
    




}
