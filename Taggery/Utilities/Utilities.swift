//
//  Utilities.swift
//  MagmaChamberCalendar
//
//  Created by Balajibabu S.G. on 09/09/17.
//  Copyright © 2017 Balajibabu S.G. All rights reserved.
//

import UIKit

class Utilities: NSObject {
    
    static var memberColorArray = [UIColor.red, UIColor.green, UIColor.blue, UIColor.brown, UIColor.darkGray, UIColor.magenta, UIColor.orange, UIColor.purple, UIColor.cyan]
    
    class func isValidEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    class  func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    class func showAlertView(_ message: String, onView view: UIViewController)  {
        
        let alert = UIAlertController(title: Constants.k_ApplicationName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    class func setBackgroungImage(_ view: UIView) {
        
        let imageView = UIImageView(image: UIImage(named: "BG.png")?.withRenderingMode(.alwaysOriginal))
        imageView.contentMode = .scaleAspectFill
        imageView.tag = 1000
        imageView.frame = view.bounds
        view.insertSubview(imageView, at: 0)
    }
    
    class func firstAndLastNameVadidation(_ Input:String, CharactersInRange range: NSRange, textField: UITextField) -> Bool {
        if (range.location == 0){
            let validString = CharacterSet(charactersIn: " !@#$%^&*()_+{}[]|\"<>,.~\\/:;?-=\'`£•¢")
            
            if Input.rangeOfCharacter(from: validString) != nil
            {
                // print(range)
                return false
            }
            else
            {
                return true
            }
        }
        else
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + Input.count - range.length
            return newLength <= 16
        }
        
    }
    class func userNameVadidation(_ Input:String, CharactersInRange range: NSRange, textField: UITextField) -> Bool {
        
            guard let text = textField.text else { return true }
            
            let newLength = text.count + Input.count - range.length
            return newLength <= 16
        
        
    }
    
    class func phoneNumberValidation(_ Input:String, CharactersInRange range: NSRange, textField: UITextField) -> Bool {
        
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + Input.characters.count - range.length
        return newLength <= 10
    }
    
    class func mandatoryText(stringToAdd:String) -> NSAttributedString {
       
        let text = stringToAdd + " *"
        let range = (text as NSString).range(of: "*")
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red , range: range)
        return attributedString
    
    }
    
    class func border(for textField: UITextField) {
        let borderColorCodeForTF: CGFloat = 235.0 / 255.0
        textField.layer.borderColor = UIColor(red: borderColorCodeForTF, green: borderColorCodeForTF, blue: borderColorCodeForTF, alpha: 1.0).cgColor
        textField.layer.borderWidth = 1.5
        textField.layer.cornerRadius = 8.0
        textField.clipsToBounds = true
        textField.layer.masksToBounds = true
    }
    
    class func setBorderColor(borderForView:Any) {
        
        (borderForView as AnyObject).layer.borderWidth = 2.0
        //        (borderForView as AnyObject).clipsToBounds = true
        (borderForView as AnyObject).layer.cornerRadius = 2.0
        (borderForView as AnyObject).layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
    class func jsonConvertor(arrayToConvert:Array<Any>) -> String {
        
        var dataProduct = Data()
        
        do{
            dataProduct = try JSONSerialization.data(withJSONObject: arrayToConvert, options: .prettyPrinted)
        }
        catch {
            print(error.localizedDescription)
        }
        return NSString(data: dataProduct, encoding: String.Encoding.utf8.rawValue)! as String
        
    }
    
    class func showActivityIndicator(navigationItem:UINavigationItem, msgText:String) {
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicatorView.frame = CGRect(x:0, y:0, width:14, height:14)
        activityIndicatorView.color = UIColor.black
        activityIndicatorView.startAnimating()
        
        let titleLabel = UILabel()
        titleLabel.text = msgText
        titleLabel.font = UIFont.italicSystemFont(ofSize: 14)
        
        let fittingSize = titleLabel.sizeThatFits(CGSize(width:200.0, height:activityIndicatorView.frame.size.height))
        titleLabel.frame = CGRect(x:activityIndicatorView.frame.origin.x + activityIndicatorView.frame.size.width + 8, y:activityIndicatorView.frame.origin.y, width:fittingSize.width, height:fittingSize.height)
        
        let titleView = UIView(frame: CGRect(x:((activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width) / 2), y:((activityIndicatorView.frame.size.height) / 2), width:(activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width), height:(activityIndicatorView.frame.size.height)))
        titleView.addSubview(activityIndicatorView)
        titleView.addSubview(titleLabel)
        
        navigationItem.titleView = titleView
    }
    
    class func hideActivityIndicator(navigationItem:UINavigationItem) {
        navigationItem.titleView = nil
    }

    
//    class func keyboardWillShow(notification: NSNotification) {
//        
//        let wrappedDict = notification.object as! [String:Any]
//        let scrollView = wrappedDict["ScrollView"] as! UIScrollView
//        let vcView = wrappedDict["VC"] as! UIViewController
//        let keyboardRectInWindow = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
//        let keyboardSize = vcView.view.convert(keyboardRectInWindow!, from: nil).size
//        var scrollInsets = scrollView.contentInset
//        scrollInsets.bottom = keyboardSize.height + 20
//        scrollView.contentInset = scrollInsets
//        scrollView.scrollIndicatorInsets = scrollInsets
//    }
//    
//    class func keyboardWillHide(notification: NSNotification) {
//       
//        let wrappedDict = notification.object as! [String:Any]
//        let scrollView = wrappedDict["ScrollView"] as! UIScrollView
//        
//        var scrollInsets = scrollView.contentInset
//        scrollInsets.bottom = 0.0
//        scrollView.contentInset = scrollInsets
//        scrollView.scrollIndicatorInsets = scrollInsets
//    }

    
    
    
}

