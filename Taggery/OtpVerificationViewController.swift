//
//  OtpVerificationViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 18/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class OtpVerificationViewController: UIViewController {

    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var resendCodeBtnOutlet: UIButton!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    
    var enteredOtp = String()
    var mobileNumber = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.setBackgroungImage(self.view)
        
        otpView.otpFieldsCount = 4
        otpView.otpFieldDefaultBorderColor = UIColor(red: 80/255.0, green: 44/255.0, blue: 150/255.0, alpha: 1.0)
        otpView.otpFieldEnteredBorderColor = UIColor.green
        otpView.otpFieldErrorBorderColor = UIColor.red
        otpView.otpFieldBorderWidth = 2
        otpView.delegate = self
        
        // Create the UI
        otpView.initalizeUI()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didReceiveOtpVerificationResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let status = responseDict["type"] as! String
        if status == "success" {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
            vc.phoneNumber = self.mobileNumber
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
            Utilities.showAlertView("Verification:\(responseDict["message"] as! String)", onView: self)
        }
    }
    
    func didReceiveReSendOtpResponse(responseDict:[String:Any]) {
        
        print("ResentResponse:",responseDict)
        
        let status = responseDict["type"] as! String
        if status == "success" {
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
//            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
            Utilities.showAlertView("ReSend:\(responseDict["message"] as! String)", onView: self)
        }
    }
    
    @IBAction func resendCodeBtnAction(_ sender: Any) {
        
        WebServiceHandler.sharedInstance.resendOtp(viewController: self, authkey: "", mobile: self.mobileNumber)
    }
    @IBAction func submitBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        vc.phoneNumber = self.mobileNumber
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension OtpVerificationViewController: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        let isEqual = (SharedVariables.sharedInstance.otpString == "12345")
        
        if isEqual {
            return true
        }
        else
        {
           return false
        }
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {

        SharedVariables.sharedInstance.otpString = otpString
        WebServiceHandler.sharedInstance.otpVerify(viewController: self, authkey: "", mobile: mobileNumber, otp: otpString)
        print("OTPString: \(SharedVariables.sharedInstance.otpString)")
    }
}



