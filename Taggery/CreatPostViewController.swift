//
//  CreatPostViewController.swift
//  Taggery
//
//  Created by Aravind on 27/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit

class CreatPostViewController: UIViewController {

    @IBOutlet weak var postImageViewOutlet: UIImageView!
    @IBOutlet weak var bottomViewOutlet: UIView!
    @IBOutlet weak var tagedLblOutlet: UILabel!
    @IBOutlet weak var textViewOutlet: UITextView!
    @IBOutlet weak var postBtnOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utilities.setBackgroungImage(self.view)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func postBtnAction(_ sender: Any) {
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
