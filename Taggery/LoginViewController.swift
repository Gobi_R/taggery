//
//  LoginViewController.swift
//  Taggery
//
//  Created by Aravind Kumar on 13/04/18.
//  Copyright © 2018 Aravind Kumar. All rights reserved.
//

import UIKit
import CoreTelephony

class LoginViewController: UIViewController {

    @IBOutlet weak var phoneNumberTxtFieldOulet: UITextField!
    @IBOutlet weak var passwordTxtFieldOutlet: UITextField!
    @IBOutlet weak var logInBtnOutlet: UIButton!
    @IBOutlet weak var countryBtnOutlet: UIButton!
    
    var countryCode = String()
    var isEyeClicked: Bool!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BG.png")!)
        
        logInBtnOutlet.layer.cornerRadius = 5;
        logInBtnOutlet.clipsToBounds = true;
        
        phoneNumberTxtFieldOulet.text = "9677877096"
        passwordTxtFieldOutlet.text = "12345678"
        
        phoneNumberTxtFieldOulet.attributedPlaceholder = NSAttributedString(string: "Phone Number",
                                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        passwordTxtFieldOutlet.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                          attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
//        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(Locale.current)
//            print(countryCode)
//
//        }
        
        let networkInfo = CTTelephonyNetworkInfo()
        
        if let carrier = networkInfo.subscriberCellularProvider,carrier.isoCountryCode != nil {
//            print("country code is: " + carrier.mobileCountryCode!);
//            //will return the actual country code
            print("ISO country code is: " + carrier.isoCountryCode!);
            let flagDetails = IsoCountryCodes.searchByCountry(name: carrier.isoCountryCode!.uppercased())
        
            let flagString = IsoCountries.flag(countryCode: flagDetails.alpha2 )

            let countryText = "\(flagString) \(String(describing: flagDetails.calling))"
            self.countryBtnOutlet.setTitle(countryText, for: .normal)
            
            var myString = "\(String(describing: flagDetails.calling))"
            countryCode = String(myString.dropFirst())
            
        }
        else
        {
            Utilities.showAlertView("No sim card inserted in your current device", onView: self)
            return
        }


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let countryValuesCount = SharedVariables.sharedInstance.selectedCountryDict.values
        
        if countryValuesCount.count > 0 {
            
            let flagString = IsoCountries.flag(countryCode: SharedVariables.sharedInstance.selectedCountryDict["iso"] as! String)
            
            print(flagString)
            
            let countryText = "\(flagString) +\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))"
            
             self.countryBtnOutlet.setTitle(countryText, for: .normal)
            
            countryCode = "\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))"
        }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func visibleEyeBtnAction(_ sender: Any) {
        if isEyeClicked == false {
            
            passwordTxtFieldOutlet.isSecureTextEntry = false
            isEyeClicked = true
        }
        else{
            
            passwordTxtFieldOutlet.isSecureTextEntry = true
            isEyeClicked = false
        }
        
        
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        
       // let phoneNumParam = "\(countryCode)\(String(describing: phoneNumberTxtFieldOulet.text!))"
//        let phoneNumParam = "\(countryCode)9677877096"
//        WebServiceHandler.sharedInstance.phoneNumberCheck(phoneNumber: phoneNumParam, viewController: self)
        
        let phoneNumParam = "\(countryCode)9677877096"
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
        vc.mobileNumber = phoneNumParam
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func countryBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectCountryViewController") as! SelectCountryViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func logInBtnAction(_ sender: Any) {
        
        let phoneNumParam = "\(countryCode)\(phoneNumberTxtFieldOulet.text!)"
        
        WebServiceHandler.sharedInstance.logInApiCall(password: passwordTxtFieldOutlet.text!, mobileNumber: phoneNumParam, viewController: self)
        
    }

    @IBAction func signUpBtnAction(_ sender: Any) {
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    // MARK:- Api Response Methods
    
    func didReceivePhoneNumberCheck(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
       // let phoneNumParam = "\(countryCode)\(String(describing: phoneNumberTxtFieldOulet.text!))"
            
            let phoneNumParam = "\(countryCode)\(phoneNumberTxtFieldOulet.text!)"
            
        WebServiceHandler.sharedInstance.sendOtp(viewController: self, authkey: "", message: "Your verification code is ##OTP##.", sender: "OTPSMS", mobile: phoneNumParam)
            
        }else{
            
        }
    }
    func didReceiveLogInResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        
        let statusCode = responseDict["StatusCode"] as! Int
        if statusCode == 200 {
            
            let userDict = responseDict["UserDetails"] as! [String:Any]
            
            Constants.userId = "\(userDict["id"]!)"
            
            UserDefaults.standard.set(true, forKey: "isLogin") //Bool
            UserDefaults.standard.set(userDict, forKey: "userDetail") //setObject
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            //            WebServiceHandler.sharedInstance.sendOtp(viewController: self, authkey: "", message: "Your verification code is ##OTP##.", sender: "OTPSMS", mobile: "\(String(describing: SharedVariables.sharedInstance.selectedCountryDict["phonecode"]!))\(phoneNumberTxtFieldOutlet.text!)")
            
        }else{
            
            Utilities.showAlertView("Registration Failed", onView: self)
        }
    }
    func didReceiveOtpResponse(responseDict:[String:Any]) {
        
        print("Response:",responseDict)
        let status = responseDict["type"] as! String
        
        if status == "success" {
           // let phoneNumParam = "\(countryCode)\(String(describing: phoneNumberTxtFieldOulet.text!))"
            let phoneNumParam = "\(countryCode)\(phoneNumberTxtFieldOulet.text!)"
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationViewController") as! OtpVerificationViewController
            vc.mobileNumber = phoneNumParam
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
        }
    }

}
